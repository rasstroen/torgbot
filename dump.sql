CREATE TABLE `messages` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) DEFAULT NULL,
  `message_type_id` int(11) DEFAULT '1',
  `network_id` int(11) DEFAULT NULL,
  `network_message_id` text COLLATE utf8_unicode_ci,
  `chat_id` text COLLATE utf8_unicode_ci,
  `author_id` text COLLATE utf8_unicode_ci,
  `text` text COLLATE utf8_unicode_ci,
  `body` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `responses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` bigint(11) DEFAULT NULL,
  `network_id` int(11) DEFAULT NULL,
  `timestamp` int(11) DEFAULT '0',
  `text` text COLLATE utf8_unicode_ci,
  `is_notfound` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `timestamp` (`timestamp`),
  KEY `network_id` (`network_id`,`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci

CREATE TABLE `message_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `updates` (
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
