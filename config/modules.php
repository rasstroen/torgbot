<?php
$modules = [];

$modules['json'] = [
    'class' => \BirdPerson\Modules\Json::class,
    'action' => 'showLastRequests',
    'template' => 'json/json.twig',
];

return $modules;