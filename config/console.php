<?php
return [
    'commands' => [
        'listen' => BirdPerson\Command\Listen::class,
        'telegram:receiver' => BirdPerson\Service\Telegram\Command\Receiver::class,
        'telegram:sender' => BirdPerson\Service\Telegram\Command\Sender::class,
        'content-api:model' => BirdPerson\Command\ContentApi\Model::class,
    ],
];
