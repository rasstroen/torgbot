<?php
$config = require_once __DIR__ . '/main.php';

if (is_readable(__DIR__ . '/console.php')) {
    $config = array_merge($config, require_once __DIR__ . '/console.php');
}

$config['telegram']['token'] = env('TELEGRAM_TOKEN');
$config['slack']['client-id'] = env('SLACK_CLIENT_ID');
$config['slack']['secret'] = env('SLACK_CLIENT_SECRET');
$config['slack']['token'] = env('SLACK_TOKEN');

return $config;