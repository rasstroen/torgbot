<?php
$logger = \bee\Core\Logger\Logger::instance();
/**
 * @var \bee\Core\Logger\Logger $logger
 */
$interval = $logger->start('config:pages');

$modules = require_once 'modules.php';

$pages = [];

$pages['index'] = new \bee\Core\Controller\Page\SimplePage(
    [
        'layout' => 'index',
        'title' => 'Торг бот',
        'css' => [
            '/static/styles.css'
        ],
        'js' => [],
        'blocks' => [

        ]
    ]
);

$pages['json'] = new \bee\Core\Controller\Page\SimplePage(
    [
        'layout' => 'json',
        'js' => [],
        'blocks' => [
            'json' => [
                $modules['json']
            ]
        ]
    ]
);

$logger->end($interval);
return $pages;