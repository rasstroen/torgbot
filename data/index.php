<?php

header('X-Accel-Expires: 0');
session_cache_limiter('public');

error_reporting(E_ALL);
ini_set('display_errors', true);


require_once __DIR__ . '/../vendor/autoload.php';


$application = new \bee\Core\Application\Web(
    realpath(__DIR__ . '/../')
);


$application->run();