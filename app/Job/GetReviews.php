<?php namespace BirdPerson\Job;

use BirdPerson\BLL\ResponsesBllTrait;
use BirdPerson\Entity\Message;
use BirdPerson\Entity\Response;
use BirdPerson\Job\ContentApi\FindModel;
use BirdPerson\Job\ContentApi\FindReviews;
use BirdPerson\Service\ContentApi\Entities\UrlTitle\Model;

class GetReviews extends AbstractJob
{
    use ResponsesBllTrait;

    /**
     * @var Message
     */
    private $message;

    /**
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }


    public function run()
    {
        $this->debug('Got message text ` ' . $this->message->getText() . ' `');

        $model = (new FindModel($this->message->getText()))->getModel();
        if ($model) {
            $reviews = (new FindReviews($model->getId()))->getReviews();
        }

        $response = new Response();
        $response->setData([
            'message_id' => $this->message->getId(),
            'network_id' => $this->message->getNetworkId(),
            'text' => $this->getResponse($model, $reviews),
        ]);
        $this->getResponsesBll()->save($response, $model ? false : true);
    }

    /**
     * @param Model $model
     *
     * @return string
     */
    private function getResponse(Model $model = null, array $reviews = null) : string
    {
        if ($model === null) {
            return 'Ничего не нашёл. Попробуйте уточнить название модели.';
        }
        $text = 'Вот <a href="' . $model->getModelLink() . '">' . $model->getName() . '</a>';
        $prices = $model->getPrices();
        $reviewsText = '';
        if ($reviews) {
            $reviewsText = '';
            foreach ($reviews['ModelOpinions']['Listing'] as $review) {
                $reviewsItemText = date('d.m.Y',
                        $review['Date']) . ', оценка: ' . ((int) $review['Mark']) . '/5' . "\n" . htmlentities(strip_tags($review['Text'])) . "\n\n";
                if (mb_strlen($reviewsItemText) > 196) {
                    $reviewsItemText = mb_substr($reviewsItemText, 0, 196) . '...' . "\n\n";
                }
                if (mb_strlen($reviewsText . $reviewsItemText) < 888) {
                    $reviewsText .= $reviewsItemText;
                }
            }
            if (mb_strlen($reviewsText) > 888) {
                $reviewsText = mb_substr($reviewsText, 0, 888) . '..';
            }
        }

        if ($prices['min'] && $prices['max']) {
            $text .= " по цене от " . $prices['min'] . ' ' . $prices['CurrencyName'];
        }
        $text .= "\n";
        if ($reviewsText) {
            $text .= "\n" . '<b>Отзывы:</b>' . "\n" . $reviewsText . ' <a href="' . $model->getReviewsLink() . '">смотреть все отзывы</a>';
        }
        return $text;
    }
}
