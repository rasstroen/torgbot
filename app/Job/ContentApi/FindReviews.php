<?php namespace BirdPerson\Job\ContentApi;

use BirdPerson\HARDCORE;
use BirdPerson\Service\ContentApi\ContentApi;
use BirdPerson\Service\ContentApi\Entities\UrlTitle\Model;
use BirdPerson\Service\ContentApi\Manager;
use BirdPerson\Service\ContentApi\Provider;
use GuzzleHttp\Client;

class FindReviews
{
    /**
     * @var int
     */
    private $modelId;


    public function __construct(int $modelId)
    {
        $this->modelId = $modelId;
    }

    /**
     * @return Model
     */
    public function getReviews()
    {
        $contentApi = new ContentApi(
            new Manager(
                new Provider(
                    new Client([
                        'headers' => [
                            'User-Agent' => 'bird_person_bot/1.0',
                        ],
                        'allow_redirects' => true,
                    ]),
                    env('CONTENT_API_TOKEN')
                )
            )
        );
        HARDCORE::$hardcore = $this->modelId;
        $reviews = $contentApi->getReviewsByModelId($this->modelId);

        return $reviews;
    }
}
