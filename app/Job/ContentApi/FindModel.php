<?php namespace BirdPerson\Job\ContentApi;

use BirdPerson\Service\ContentApi\ContentApi;
use BirdPerson\Service\ContentApi\Entities\UrlTitle\Model;
use BirdPerson\Service\ContentApi\Manager;
use BirdPerson\Service\ContentApi\Provider;
use GuzzleHttp\Client;

class FindModel
{
    /**
     * @var string
     */
    private $title;

    /**
     * @param string $title
     */
    public function __construct(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        $contentApi = new ContentApi(
            new Manager(
                new Provider(
                    new Client([
                        'headers' => [
                            'User-Agent' => 'bird_person_bot/1.0',
                        ],
                        'allow_redirects' => true,
                    ]),
                    env('CONTENT_API_TOKEN')
                )
            )
        );

        $models = $contentApi->getModelsBySearch($this->title);
        $model = reset($models);

        return $model ? reset($model) : null;
    }
}
