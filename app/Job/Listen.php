<?php namespace BirdPerson\Job;

use BirdPerson\BLL\MessagesBllTrait;
use BirdPerson\BLL\UpdatesBllTrait;
use BirdPerson\Exceptions\CommonException;
use BirdPerson\Service\Messages;
use BirdPerson\Service\Updates;

class Listen extends AbstractJob
{
    use MessagesBllTrait;
    use UpdatesBllTrait;

    public function run()
    {
        $debug = $this->debug;
        $lastId = $this->getUpdatesBll()->get(Updates::LAST_MESSAGE_ID);
        $messages = $this->getMessagesBll()->getNew((int) $lastId);

        try {
            foreach ($messages as $message) {

                $this->debug('Got message id = `' . $message->getId() . '`');
                $job = Messages::getJobForMessage($message);
                $job->setDebug($debug);
                $job->run();
                $this->getUpdatesBll()->set(Updates::LAST_MESSAGE_ID, $message->getId());
            }
        } catch (CommonException $exception) {
            // todo create default response
        }
    }
}
