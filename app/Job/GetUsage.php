<?php namespace BirdPerson\Job;

use BirdPerson\BLL\ResponsesBllTrait;
use BirdPerson\Entity\Message;
use BirdPerson\Entity\Response;

class GetUsage extends AbstractJob
{
    use ResponsesBllTrait;

    /**
     * @var Message
     */
    private $message;

    /**
     * @var string
     */
    private $text;

    private static $usageText = 'Для получения отзывов о товаре, просто напишите мне название товара. Например, <i>@torgmailru_bot</i> <b>Apple Iphone 5s</b>';

    /**
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    public function run()
    {
        $this->debug('Got message text ` ' . $this->text . ' `');

        // todo call content api
        // todo get response by content api response

        $response = new Response();
        $response->setData([
            'message_id' => $this->message->getId(),
            'network_id' => $this->message->getNetworkId(),
            'text' => static::$usageText,
        ]);
        $this->getResponsesBll()->save($response);
    }
}
