<?php namespace BirdPerson\Job;

use BirdPerson\BLL\ResponsesBllTrait;
use BirdPerson\Entity\Message;
use BirdPerson\Entity\Response;

class GetBash extends AbstractJob
{
    use ResponsesBllTrait;

    /**
     * @var Message
     */
    private $message;

    /**
     * @var string
     */
    private $text;

    /**
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    public function run()
    {
        $this->debug('Got message text ` ' . $this->text . ' `');

        $text = file_get_contents('http://bash.im/forweb/?u');
        preg_match_all('/(.*);\"\>(.*)<\'\s\+\s\'\/div\>\<\'(.*)/isU', $text, $matches);
        $text = str_replace('<\' + \'br>', "\n", $matches[2][0]);

        $response = new Response();
        $response->setData([
            'message_id' => $this->message->getId(),
            'network_id' => $this->message->getNetworkId(),
            'text' => $text,
        ]);
        $this->getResponsesBll()->save($response);
    }
}
