<?php namespace BirdPerson\Job;

abstract class AbstractJob
{
    /**
     * @var Callable
     */
    protected $debug;

    abstract public function run();

    /**
     * @param callable $debug
     */
    public function setDebug(Callable $debug)
    {
        $this->debug = $debug;
    }

    /**
     * @param string $message
     */
    public function debug(string $message)
    {
        if (is_callable($this->debug)) {
            $debug = $this->debug;
            $debug($message);
        }
    }
}
