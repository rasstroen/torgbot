<?php namespace BirdPerson\Command;

use BirdPerson\Job\Listen as ListenJob;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Listen extends Command
{
    const USLEEP_TIME = 100000;

    protected function configure()
    {
        $this
            ->setName('listen')
            ->setDescription('Do the job.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $job = new ListenJob();
        $job->setDebug(function ($message) use ($output) {
            $output->writeln($message);
        });
        while (true) {
            $job->run();
            usleep(self::USLEEP_TIME);
        }
    }
}
