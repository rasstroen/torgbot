<?php namespace BirdPerson\Command\ContentApi;

use BirdPerson\Job\ContentApi\FindModel;
use BirdPerson\Job\ContentApi\FindReviews;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Model extends Command
{
    protected function configure()
    {
        $this
            ->setName('content-api:model')
            ->setDescription('Finds model in content api by title.')
            ->addArgument(
                'title',
                InputArgument::REQUIRED,
                'Model to find?'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $job = new FindModel($input->getArgument('title'));
        try {
            $model = $job->getModel();
        } catch (\Exception $e) {
            $model = null;
        }
        if ($model === null) {
            $output->writeln('Can`t find model by given title.');
            die;
        }

        if ($model) {
            $reviews = (new FindReviews($model->getId()))->getReviews();
            $reviewsText = '';
            foreach ($reviews as $review) {
                $reviewsText .= mb_substr(0, 255, htmlspecialchars(strip_tags($review['Text']))) . "\n";
            }
            $reviewsText = mb_substr($reviewsText, 0, 300);
        }



        $text = '<a href="' . $model->getModelLink() . '">' . $model->getName() . '</a>';
        $prices = $model->getPrices();

        if ($prices['min'] && $prices['max']) {
            $text .= "\n " . ($model->getOffersTotal() ? $model->getOffersTotal() . " предложений" : "") . " от " . $prices['min'] . ' ' . $prices['CurrencyName'] . ' ' . $model->getSellers();
        }

        $output->writeln(
            $text
        );
    }
}
