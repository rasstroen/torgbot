<?php namespace BirdPerson\BLL;

use bee\Core\Module\BLL\DbTable;
use bee\Traits\Classes\Database\FactoryTrait;
use bee\Traits\Core\InjectableComponentTrait;
use BirdPerson\Entity\Message;
use BirdPerson\Entity\MessagesCollection;
use BirdPerson\Service\TranscoderFactory;

class Messages extends DbTable
{
    use InjectableComponentTrait;
    use FactoryTrait;

    protected function getTableName():string
    {
        return 'messages';
    }

    protected function getIndexFieldName():string
    {
        return 'id';
    }

    /**
     * @param Message $message
     *
     * @return int
     */
    public function saveMessage(Message $message)
    {
        return $this->getConnection()->web->insert('messages', [
            'network_message_id' => $message->getId(),
            'message_type_id' => $message->getMessageTypeId(),
            'network_id' => $message->getNetworkId(),
            'timestamp' => $message->getTimestamp(),
            'chat_id' => $message->getChatId(),
            'author_id' => $message->getAuthorId(),
            'text' => $message->getText(),
            'body' => $message->getBody(),
        ]);
    }

    /**
     * @param int $count
     *
     * @return array
     */
    public function getLatestResponses(int $count = 10)
    {
        $query = '
            SELECT
              *, R.text as reply_text, R.id as reply_id
            FROM
              `responses` R
            LEFT JOIN
              `messages` M
            ON
              M.id=R.message_id
            WHERE
              R.`timestamp`=0 AND
              R.`network_id`= ?
            ORDER BY
              M.timestamp
            LIMIT ' . $count;
        return $this->getConnection()->web->selectAll($query , [TranscoderFactory::NETWORK_ID_TELEGRAM]);
    }

    public function getLatestMessages(){
        $query = 'select M.id as id,M.text as text from messages M join responses R ON M.id=R.message_id WHERE R.is_notfound=0 order by M.timestamp desc limit 10';
        return $this->getConnection()->web->selectAll($query);
    }

    public function setReplyTime(int $responseId)
    {
        return $this->getConnection()->web->insert('responses', ['id' => $responseId, 'timestamp' => time()],
            ['timestamp']);
    }

    public function getAll()
    {
        return $this->getConnection()->web->selectAll('
            SELECT * FROM messages
        ');
    }

    /**
     * @param int $id
     *
     * @return Message[]
     */
    public function getNew(int $id)
    {
        $data = $this->getConnection()->web->selectAll('
            SELECT *
            FROM messages
            WHERE id > ?
        ', [
            $id,
        ]);

        $collection = new MessagesCollection();
        $collection->setData($data);

        return $collection;
    }
}
