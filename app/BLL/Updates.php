<?php namespace BirdPerson\BLL;

use bee\Core\Module\BLL\DbTable;
use bee\Traits\Core\InjectableComponentTrait;

class Updates extends DbTable
{
    use InjectableComponentTrait;

    protected function getTableName():string
    {
        return 'updates';
    }

    protected function getIndexFieldName():string
    {
        return 'key';
    }

    /**
     * @param string $key
     *
     * @return string
     */
    public function get(string $key) : string
    {
        return (string)$this->getConnection()->web->selectValue('
            SELECT `value`
            FROM `' . $this->getTableName() . '`
            WHERE
                `key`= ? 
        ', [
            $key,
        ]);
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function set(string $key, string $value)
    {
        $this->getConnection()->web->insert($this->getTableName(), [
            '`key`' => $key,
            'value' => $value,
        ], [
            'value',
        ]);
    }
}
