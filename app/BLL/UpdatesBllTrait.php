<?php namespace BirdPerson\BLL;

trait UpdatesBllTrait
{
    /**
     * @return Updates
     */
    public function getUpdatesBll()
    {
        return Updates::instance();
    }
}
