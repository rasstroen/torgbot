<?php namespace BirdPerson\BLL;

use bee\Core\Module\BLL\DbTable;
use bee\Traits\Classes\Database\FactoryTrait;
use bee\Traits\Core\InjectableComponentTrait;
use BirdPerson\Entity\Response;

class Responses extends DbTable
{
    use InjectableComponentTrait;
    use FactoryTrait;

    protected function getTableName():string
    {
        return 'responses';
    }

    protected function getIndexFieldName():string
    {
        return 'id';
    }

    /**
     * @param Response $response
     *
     * @return int
     */
    public function save(Response $response, $isNotFound = true)
    {
        return $this->getConnection()->web->insert($this->getTableName(), [
            'message_id' => $response->getMessageId(),
            'network_id' => $response->getNetworkId(),
            'timestamp' => $response->getTimestamp(),
            'text' => $response->getText(),
            'is_notfound' => $isNotFound ? 1 : 0
        ]);
    }
}
