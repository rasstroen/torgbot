<?php namespace BirdPerson\BLL;

trait MessagesBllTrait
{
    /**
     * @return Messages
     */
    public function getMessagesBll()
    {
        return Messages::instance();
    }
}
