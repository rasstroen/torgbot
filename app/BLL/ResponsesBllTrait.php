<?php namespace BirdPerson\BLL;

trait ResponsesBllTrait
{
    /**
     * @return Responses
     */
    public function getResponsesBll()
    {
        return Responses::instance();
    }
}
