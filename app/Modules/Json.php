<?php namespace BirdPerson\Modules;

use bee\Core\Module\Base;
use bee\Traits\Core\ProjectConfigurationTrait;
use BirdPerson\BLL\MessagesBllTrait;

class Json extends Base
{
    use MessagesBllTrait;
    use ProjectConfigurationTrait;

    public function actionShowLastRequests()
    {
        return ['json' => json_encode($this->getMessagesBll()->getLatestMessages())];
    }
}