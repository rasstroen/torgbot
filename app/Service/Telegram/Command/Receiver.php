<?php namespace BirdPerson\Service\Telegram\Command;

use BirdPerson\Service\Telegram\TelegramReceiverTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Receiver extends Command
{
    const USLEEP_TIME = 100000;

    use TelegramReceiverTrait;

    protected function configure()
    {
        $this
            ->setName('telegram:receiver')
            ->setDescription('Receive updates.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Start receiving.');
        $receiver = $this->getTelegramReceiver();
        while (true) {
            usleep(self::USLEEP_TIME);
            $receiver->receive($output);
        }
    }
}
