<?php namespace BirdPerson\Service\Telegram\Command;

use BirdPerson\Service\Telegram\TelegramSenderTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Sender extends Command
{
    const USLEEP_TIME = 100000;

    use TelegramSenderTrait;

    protected function configure()
    {
        $this
            ->setName('telegram:sender')
            ->setDescription('Send replies.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Start sending.');
        $sender = $this->getTelegramSender();
        while (true) {
            usleep(self::USLEEP_TIME);
            $sender->send($output);
        }
    }
}
