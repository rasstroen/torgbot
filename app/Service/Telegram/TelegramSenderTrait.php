<?php namespace BirdPerson\Service\Telegram;

trait TelegramSenderTrait
{
    /**
     * @return Sender
     */
    public function getTelegramSender()
    {
        return Sender::instance();
    }
}
