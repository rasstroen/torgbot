<?php namespace BirdPerson\Service\Telegram;

trait TelegramReceiverTrait
{
    /**
     * @return Receiver
     */
    public function getTelegramReceiver()
    {
        return Receiver::instance();
    }
}
