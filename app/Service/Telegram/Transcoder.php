<?php namespace BirdPerson\Service\Telegram;

use bee\Traits\Core\InjectableComponentTrait;
use BirdPerson\Entity\Message;
use BirdPerson\Service\Messages;
use BirdPerson\Service\TranscoderFactory;
use Telegram\Bot\Objects\Update;

class Transcoder
{

    const MESSAGE_TYPE_GET_REVIEWS = 1;

    use InjectableComponentTrait;

    /**
     * @param Update $update
     * @return Message
     */
    public function getMessage(Update $update, string $botName)
    {
        $message = new Message();
        $telegramMessage = $update->get('message');
        $sender = $telegramMessage->get('from');
        $chat = $telegramMessage->get('chat');
        $message->setAuthorId($sender->get('id'));
        $message->setBody(json_encode($update));
        $message->setChatId($chat->get('id'));
        $message->setNetworkId(TranscoderFactory::NETWORK_ID_TELEGRAM);
        $message->setText($this->getText($telegramMessage->get('text'), $botName));
        $message->setTimestamp($telegramMessage->get('date'));
        $message->setId($telegramMessage->get('message_id'));
        $message->setMessageTypeId($this->getMessageTypeId($message));

        return $message;
    }

    private function getMessageTypeId(Message $message)
    {
        $words = explode(' ', $message->getText(), 1);
        $firstWord = trim(reset($words));
        $command = '';
        if (substr($firstWord, 0, 1) === '/') {
            $command = strtolower($firstWord);
        }
        switch ($command) {
            case '':
                return Messages::TYPE_GET_REVIEWS;
                break;
            case '/help':
                return Messages::TYPE_GET_USAGE;
                break;
            case '/bash':
                return Messages::TYPE_GET_BASH;
                break;
            default:
                return Messages::TYPE_GET_USAGE;
                break;
        }
    }

    private function getText(string $text = null, string $botName = null)
    {
        if (null === $text) {
            return '';
        }
        $text = trim(str_replace('@' . $botName, '', $text));
        return $text;
    }
}
