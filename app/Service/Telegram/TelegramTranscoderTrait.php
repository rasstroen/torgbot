<?php namespace BirdPerson\Service\Telegram;

trait TelegramTranscoderTrait
{
    /**
     * @return Transcoder
     */
    public function getTelegramTranscoder()
    {
        return Transcoder::instance();
    }
}
