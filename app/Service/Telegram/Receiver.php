<?php namespace BirdPerson\Service\Telegram;

use bee\Traits\Core\InjectableComponentTrait;
use bee\Traits\Core\ProjectConfigurationTrait;
use BirdPerson\BLL\MessagesBllTrait;
use BirdPerson\BLL\UpdatesBllTrait;
use BirdPerson\Service\Updates;
use Symfony\Component\Console\Output\OutputInterface;
use Telegram\Bot\Api;

class Receiver
{
    const LONG_POOL_SECONDS_TIMEOUT = 10;

    use InjectableComponentTrait;
    use ProjectConfigurationTrait;
    use TelegramTranscoderTrait;
    use MessagesBllTrait;
    use UpdatesBllTrait;

    /**
     * @var int
     */
    private $lastUpdateId = 0;

    /**
     * @var Api
     */
    private $telegram;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var string
     */
    private $botName;

    /**
     * @return int
     */
    public function getLastUpdateId()
    {
        return $this->lastUpdateId;
    }

    /**
     * @param int $lastUpdateId
     */
    public function setLastUpdateId($lastUpdateId)
    {
        $this->lastUpdateId = $lastUpdateId;
    }

    private function getTelegram()
    {
        if (null === $this->telegram) {
            $this->telegram = new Api($this->configuration()->getSetting('telegram.token'));
            $response = $this->telegram->getMe();
            $botId = $response->getId();
            $firstName = $response->getFirstName();
            $username = $response->getUsername();
            $this->botName = $username;
            $this->debug('bot: ' . $firstName . ' ' . $username . ', id: ' . $botId);
        }
        return $this->telegram;
    }

    private function debug($message)
    {
        $this->output->writeln($message);
    }

    /**
     * @return string
     */
    private function getBotName()
    {
        return $this->botName;
    }

    public function receive(OutputInterface $output)
    {
        $this->output = $output;
        $telegram = $this->getTelegram();

        $lastUpdateId = $this->getUpdatesBll()->get(Updates::LAST_RECEIVED_MESSAGE_ID);
        $this->setLastUpdateId($lastUpdateId);

        $updates = $telegram->getUpdates([
            'offset' => $this->getLastUpdateId(),
            'timeout' => self::LONG_POOL_SECONDS_TIMEOUT
        ]);

        foreach ($updates as $update) {
            $updateId = $update->getUpdateId();
            if ($updateId <= $this->getLastUpdateId()) {
                continue;
            }
            $this->setLastUpdateId($update->getUpdateId());
            $message = $this->getTelegramTranscoder()->getMessage($update, $this->getBotName());
            if (mb_strlen($message->getText()) < 3) {
                $message->setText('');
            }
            $this->debug('saving message' . $message->getId());
            $this->getMessagesBll()->saveMessage($message);
        }
        if ($lastUpdateId != $this->getLastUpdateId()) {
            $lastUpdateId = $this->getLastUpdateId();
            $this->getUpdatesBll()->set(Updates::LAST_RECEIVED_MESSAGE_ID, $this->getLastUpdateId());
        }
    }
}
