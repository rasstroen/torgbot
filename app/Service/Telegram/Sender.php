<?php namespace BirdPerson\Service\Telegram;

use bee\Traits\Core\InjectableComponentTrait;
use bee\Traits\Core\ProjectConfigurationTrait;
use BirdPerson\BLL\MessagesBllTrait;
use Symfony\Component\Console\Output\OutputInterface;
use Telegram\Bot\Api;

class Sender
{
    const USLEEP_TIME = 100000;

    use InjectableComponentTrait;
    use ProjectConfigurationTrait;
    use TelegramTranscoderTrait;
    use MessagesBllTrait;

    /**
     * @var Api
     */
    private $telegram;

    /**
     * @var OutputInterface
     */
    private $output;

    private function getTelegram()
    {
        if (null === $this->telegram) {
            $this->telegram = new Api($this->configuration()->getSetting('telegram.token'));
            $response = $this->telegram->getMe();
            $botId = $response->getId();
            $firstName = $response->getFirstName();
            $username = $response->getUsername();
            $this->debug('bot: ' . $firstName . ' ' . $username . ', id: ' . $botId);
        }
        return $this->telegram;
    }

    private function debug($message)
    {
        $this->output->writeln($message);
    }

    public function send(OutputInterface $output)
    {
        $this->output = $output;
        $this->debug('Selecting messages');
        $responseRows = $this->getMessagesBll()->getLatestResponses(10);
        foreach ($responseRows as $row) {
            echo $row['reply_text'];
            try {
                $this->getTelegram()->sendMessage([
                    'chat_id' => $row['chat_id'],
                    'text' => (strip_tags($row['reply_text'],'<b><a>')),
                    'parse_mode' => 'HTML',
                    'disable_web_page_preview' => true,
                    'reply_to_message_id' => $row['network_message_id']
                ]);
            } catch (\Exception $e) {
                echo str_replace(['&nbsp;','<BR>','<br >','<br/>'], '', $row['reply_text']);
                echo $e->getMessage();
            }
            $this->getMessagesBll()->setReplyTime($row['reply_id']);
        }
        usleep(self::USLEEP_TIME);
    }

}
