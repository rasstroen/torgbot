<?php namespace BirdPerson\Service\ContentApi\Requests;

class UrlTitle extends AbstractRequest
{
    const TYPE = 'search_url_title';

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $url;

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return http_build_query([
            'title' => $this->title,
            'url' => $this->url,
        ]);
    }
}
