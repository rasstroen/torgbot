<?php namespace BirdPerson\Service\ContentApi\Requests;

class Search extends AbstractRequest
{
    const TYPE = 'search';

    /**
     * @var string
     */
    private $query;

    /**
     * @var int
     */
    private $geoId;

    /**
     * @var string
     */
    private $catalogIds;

    /**
     * @param string $query
     */
    public function setQuery(string $query)
    {
        $this->query = $query;
    }

    /**
     * @param int $geoId
     */
    public function setGeoId($geoId)
    {
        $this->geoId = $geoId;
    }

    /**
     * @param string $catalogIds
     */
    public function setCatalogIds($catalogIds)
    {
        $this->catalogIds = $catalogIds;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return http_build_query([
            'query' => $this->query,
            //'category_id' => $this->catalogIds,
            'geo_id' => $this->geoId,
            'only_models' => true
        ]);
    }
}
