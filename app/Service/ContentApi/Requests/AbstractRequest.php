<?php namespace BirdPerson\Service\ContentApi\Requests;

abstract class AbstractRequest
{
    const TYPE = '';

    /**
     * @return string
     */
    public function getType()
    {
        return static::TYPE;
    }

    protected function validate()
    {
    }

    /**
     * @return string
     */
    abstract public function getData();
}
