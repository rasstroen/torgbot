<?php namespace BirdPerson\Service\ContentApi\Requests;

class Reviews extends AbstractRequest
{
    const TYPE = 'reviews';

    /**
     * @var int
     */
    private $geoId;

    /**
     * @var string
     */
    private $modelId;

    /**
     * @param string $query
     */
    public function setQuery(string $query)
    {
        $this->query = $query;
    }

    /**
     * @param int $geoId
     */
    public function setGeoId($geoId)
    {
        $this->geoId = $geoId;
    }

    /**
     * @param $modelId
     */
    public function setModelId($modelId)
    {
        $this->modelId = $modelId;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return http_build_query([
            'geo_id' => $this->geoId
        ]);
    }
}
