<?php namespace BirdPerson\Service\ContentApi\Entities\UrlTitle;

use BirdPerson\Service\ContentApi\Entities\AbstractEntity;

class Model extends AbstractEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $vendor;

    /**
     * @var int
     */
    private $vendorId;

    /**
     * @var int
     */
    private $rating;

    /**
     * @var string
     */
    private $addReviewLink;

    /**
     * @var string
     */
    private $reviewsLink;

    /**
     * @var int
     */
    private $reviewsTotal;

    /**
     * @var int
     */
    private $categoryId;

    /**
     * @var int
     */
    private $offersTotal;

    /**
     * @var bool
     */
    private $isNew;

    /**
     * @var array
     */
    private $prices;

    /**
     * @var string
     */
    private $mainPhoto;

    /**
     * @var array
     */
    private $photos;

    /**
     * @var string
     */
    private $modelLink;

    /**
     * @var string
     */
    private $discussionLink;

    /**
     * @var int
     */
    private $discussionTotal;

    /**
     * @var array
     */
    private $reviews;

    /**
     * @var array
     */
    private $sellers;

    /**
     * @var int
     */
    private $sellersTotal;

    /**
     * @param array $data
     *
     * @return Model|null
     */
    public function setData(array $data)
    {
        if (!isset($data['SearchResult'], $data['SearchResult']['Model'])) {
            return null;
        }

        $data = $data['SearchResult']['Model'];

        if (empty($data)) {
            return null;
        }

        if (!empty($data['Id'])) {
            $this->id = (int)$data['Id'];
        }
        if (!empty($data['Name'])) {
            $this->name = $data['Name'];
        }
        if (!empty($data['Description'])) {
            $this->description = $data['Description'];
        }
        if (!empty($data['Vendor'])) {
            $this->vendor = $data['Vendor'];
        }
        if (!empty($data['VendorId'])) {
            $this->vendorId = (int)$data['VendorId'];
        }
        if (!empty($data['Rating'])) {
            $this->rating = (int)$data['Rating'];
        }
        if (!empty($data['AddReviewLink'])) {
            $this->addReviewLink = $data['AddReviewLink'];
        }
        if (!empty($data['ReviewsLink'])) {
            $this->reviewsLink = $data['ReviewsLink'];
        }
        if (!empty($data['ReviewsTotal'])) {
            $this->reviewsTotal = (int)$data['ReviewsTotal'];
        }
        if (!empty($data['CategoryId'])) {
            $this->categoryId = (int)$data['CategoryId'];
        }
        if (!empty($data['OffersTotal'])) {
            $this->offersTotal = (int)$data['OffersTotal'];
        }
        if (!empty($data['IsNew'])) {
            $this->isNew = (bool)$data['IsNew'];
        }
        // todo
        if (!empty($data['Prices'])) {
            $this->prices = $data['Prices'];
        }
        if (!empty($data['MainPhoto'])) {
            $this->mainPhoto = $data['MainPhoto'];
        }
        // todo
        if (!empty($data['Photos'])) {
            $this->photos = $data['Photos'];
        }
        if (!empty($data['ModelLink'])) {
            $this->modelLink = $data['ModelLink'];
        }
        if (!empty($data['DiscussionLink'])) {
            $this->discussionLink = $data['DiscussionLink'];
        }
        if (!empty($data['DiscussionTotal'])) {
            $this->discussionTotal = (int)$data['DiscussionTotal'];
        }
        // todo
        if (!empty($data['Reviews'])) {
            $this->reviews = $data['Reviews'];
        }
        // todo
        if (!empty($data['Sellers'])) {
            $this->sellers = $data['Sellers'];
        }
        if (!empty($data['SellersTotal'])) {
            $this->sellersTotal = (int)$data['SellersTotal'];
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getVendor() : string
    {
        return $this->vendor;
    }

    /**
     * @return int
     */
    public function getVendorId() : int
    {
        return $this->vendorId;
    }

    /**
     * @return int
     */
    public function getRating() : int
    {
        return $this->rating;
    }

    /**
     * @return string
     */
    public function getAddReviewLink() : string
    {
        return $this->addReviewLink;
    }

    /**
     * @return string
     */
    public function getReviewsLink() : string
    {
        return $this->reviewsLink;
    }

    /**
     * @return int
     */
    public function getReviewsTotal() : int
    {
        return $this->reviewsTotal;
    }

    /**
     * @return int
     */
    public function getCategoryId() : int
    {
        return $this->categoryId;
    }

    /**
     * @return int
     */
    public function getOffersTotal() : int
    {
        return $this->offersTotal;
    }

    /**
     * @return bool
     */
    public function isIsNew() : bool
    {
        return $this->isNew;
    }

    /**
     * @return array
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @return string
     */
    public function getMainPhoto() : string
    {
        return $this->mainPhoto;
    }

    /**
     * @return array
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @return string
     */
    public function getModelLink() : string
    {
        return $this->modelLink;
    }

    /**
     * @return string
     */
    public function getDiscussionLink() : string
    {
        return $this->discussionLink;
    }

    /**
     * @return int
     */
    public function getDiscussionTotal() : int
    {
        return $this->discussionTotal;
    }

    /**
     * @return array
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @return array
     */
    public function getSellers()
    {
        return $this->sellers;
    }

    /**
     * @return int
     */
    public function getSellersTotal() : int
    {
        return $this->sellersTotal;
    }
}
