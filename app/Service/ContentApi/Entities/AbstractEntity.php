<?php namespace BirdPerson\Service\ContentApi\Entities;

abstract class AbstractEntity
{
    protected $data = [];

    /**
     * @param array $data
     */
    abstract public function setData(array $data);
}
