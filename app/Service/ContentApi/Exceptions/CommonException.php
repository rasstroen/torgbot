<?php namespace BirdPerson\Service\ContentApi\Exceptions;

use Exception;

class CommonException extends Exception
{
    /**
     * @param string         $message
     * @param int            $code
     * @param Exception|null $previous
     */
    public function __construct(string $message = '', int $code = 0, Exception $previous = null)
    {
        $message = 'Content Api exception: ' . $message;

        parent::__construct($message, $code, $previous);
    }
}
