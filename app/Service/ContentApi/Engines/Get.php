<?php namespace BirdPerson\Service\ContentApi\Engines;

use BirdPerson\HARDCORE;
use GuzzleHttp\Psr7\Request;

class Get extends AbstractEngine
{
    const TYPE = 'get';

    public function request()
    {
        $request = new Request('GET', $this->getUrl(), [
            'Authorization' => $this->authorizationToken,
        ]);

        $this->response = $this->httpClient->send($request);
    }

    /**
     * @return string
     */
    protected function getUrl()
    {
        // @todo!
        if (strpos($this->method, '#')) {
            $this->method = str_replace('#', HARDCORE::$hardcore, $this->method);
        }
        $url = $this->baseUrl . $this->method . '.' . $this->responseType . '?' . $this->getDataUrl();
        return $url;
    }

    /**
     * @return string
     */
    protected function getDataUrl()
    {
        if (!is_array($this->data)) {
            return $this->data;
        }

        return '';
    }
} 