<?php namespace BirdPerson\Service\ContentApi\Engines;

use BirdPerson\Service\ContentApi\Response\Handlers\AbstractHandler;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractEngine
{
    /**
     * @var AbstractHandler
     */
    protected $responseHandler;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $authorizationToken;

    /**
     * @var string
     */
    protected $responseType;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @param AbstractHandler $responseHandler
     */
    public function __construct(AbstractHandler $responseHandler)
    {
        $this->responseHandler = $responseHandler;
    }

    /**
     * @param string $method
     *
     * @return AbstractEngine
     */
    public function setMethod($method) : AbstractEngine
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @param mixed $data
     *
     * @return AbstractEngine
     */
    public function setData($data) : AbstractEngine
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param Client $httpClient
     *
     * @return AbstractEngine
     */
    public function setHttpClient(Client $httpClient) : AbstractEngine
    {
        $this->httpClient = $httpClient;

        return $this;
    }

    /**
     * @param string $baseUrl
     *
     * @return AbstractEngine
     */
    public function setBaseUrl($baseUrl) : AbstractEngine
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    /**
     * @param string $authorizationToken
     *
     * @return AbstractEngine
     */
    public function setAuthorizationToken(string $authorizationToken) : AbstractEngine
    {
        $this->authorizationToken = $authorizationToken;

        return $this;
    }

    /**
     * @param string $responseType
     *
     * @return AbstractEngine
     */
    public function setResponseType(string $responseType) : AbstractEngine
    {
        $this->responseType = $responseType;

        return $this;
    }

    /**
     * @return Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @return bool
     */
    public function hasErrors() : bool
    {
        // todo
        return false;
//        return !$this->httpClient->isOk();
    }

    abstract public function request();

    /**
     * @return array
     */
    public function response() : array
    {
        return $this->responseHandler->handle(
            $this->response->getBody()->getContents()
        );
    }
}
