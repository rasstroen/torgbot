<?php namespace BirdPerson\Service\ContentApi\Collections;

use BirdPerson\Service\ContentApi\Entities\UrlTitle\Model;

class Models extends AbstractCollection
{

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data)
    {
        foreach ($data['searchResult']['Listing'] as $data) {
            $model = new Model();
            $model->setData(['SearchResult' => ['Model' => $data['Model']]]);
            $this->add($model);
        }
        return $this;
    }
}