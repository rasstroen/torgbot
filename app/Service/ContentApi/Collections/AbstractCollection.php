<?php namespace BirdPerson\Service\ContentApi\Collections;

use BirdPerson\Service\ContentApi\Entities\AbstractEntity;
use Countable;
use Iterator;

abstract class AbstractCollection extends AbstractEntity implements Iterator, Countable
{
    /**
     * @var array
     */
    protected $objects = [];

    /**
     * @param AbstractEntity $object
     */
    protected function add(AbstractEntity $object)
    {
        $this->objects[] = $object;
    }
    
    /**
     * @return AbstractEntity
     */
    public function current() : AbstractEntity
    {
        return current($this->objects);
    }

    /**
     * @return AbstractEntity|bool
     */
    public function next() : AbstractEntity
    {
        return next($this->objects);
    }

    /**
     * @return mixed
     */
    public function key()
    {
        return key($this->objects);
    }

    /**
     * @return bool
     */
    public function valid() : bool
    {
        $key = key($this->objects);

        return ($key !== null && $key !== false);
    }


    public function rewind()
    {
        reset($this->objects);
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->objects);
    }
}
