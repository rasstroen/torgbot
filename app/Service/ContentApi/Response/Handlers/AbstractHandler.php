<?php namespace BirdPerson\Service\ContentApi\Response\Handlers;

abstract class AbstractHandler
{
    /**
     * @param string $response
     *
     * @return array
     */
    public function handle(string $response) : array
    {
        return json_decode($response, true);
    }
}
