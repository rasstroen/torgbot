<?php namespace BirdPerson\Service\ContentApi;

use BirdPerson\Service\ContentApi\Collections\Models;
use BirdPerson\Service\ContentApi\Entities\UrlTitle\Model;
use BirdPerson\Service\ContentApi\Requests\AbstractRequest;
use BirdPerson\Service\ContentApi\Requests\Reviews;
use BirdPerson\Service\ContentApi\Requests\Search;
use BirdPerson\Service\ContentApi\Requests\UrlTitle;

class ContentApi
{
    /**
     * @var Manager
     */
    private $manager;

    /**
     * @param Manager $manager
     */
    public function __construct($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $title
     *
     * @return Model|null
     */
    public function getModelByTitle(string $title)
    {
        $request = new UrlTitle();
        $request->setTitle($title);
        $request->setUrl('bird_person_bot');

        return (new Model())->setData(
            $this->process($request)
        );
    }

    public function getReviewsByModelId(int $modelId)
    {
        $request = new Reviews();
        $request->setModelId($modelId);
        $request->setGeoId(25);
        return $this->process($request);
    }

    public function getModelsBySearch(string $title)
    {
        $request = new Search();
        $request->setQuery($title);
        $request->setCatalogIds('844,844,844,844,337,3033,337,109,109,109,3879,3879,3879,3879,120,135,120,132,2624,2624,119,2064,115,1131,115,115,97,97,97,97');
        $request->setGeoId(25);

        return (new Models())->setData(
            $this->process($request)
        );
    }

    /**
     * @param AbstractRequest $request
     *
     * @return array
     */
    private function process(AbstractRequest $request)
    {
        try {
            $response = $this->manager->process($request);
            return $response->getData();
        } catch (\Exception $exception) {
            echo $exception->getMessage(), PHP_EOL;
        }

        return [];
    }
}
