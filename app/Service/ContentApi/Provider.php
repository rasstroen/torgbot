<?php namespace BirdPerson\Service\ContentApi;

use BirdPerson\Service\ContentApi\Engines\AbstractEngine;
use BirdPerson\Service\ContentApi\Exceptions\CommonException;
use GuzzleHttp\Client;

class Provider
{
    const MAX_ITERATIONS = 5;
    const SLEEP_SECONDS = 2;

    private static $url = 'http://content.api.torg.mail.ru/';
    private static $version = '1.0';
    private static $responseType = 'json';

    private $iterationsCount = 1;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var string
     */
    private $token;

    /**
     * @param Client $httpClient
     * @param string $token
     */
    public function __construct(Client $httpClient, string $token)
    {
        $this->httpClient = $httpClient;
        $this->token = $token;
    }

    /**
     * @param AbstractEngine $engine
     *
     * @return string
     * @throws CommonException
     */
    public function process(AbstractEngine $engine)
    {
        $engine
            ->setHttpClient($this->httpClient)
            ->setAuthorizationToken($this->token)
            ->setResponseType(self::$responseType)
            ->setBaseUrl($this->getUrl())
            ->request();

        if ($engine->hasErrors()) {
            // Повторный запрос после паузы, если код ответа '0'
            if ($this->needRepeat($engine)) {
                $this->processRepeat($engine);
            }

//            throw new CommonException(
//                'Not 200 cUrl code: "' . $engine->getHttpClient()->code . '", request url: "' . parse_url($engine->getHttpClient()->getUrl())['path'] . '"'
//            );
        }

        return $engine->response();
    }

    /**
     * @return string
     */
    private function getUrl() : string
    {
        $url = parse_url(self::$url);

        return $url['scheme'] . '://' . $url['host'] . $url['path'] . self::$version . '/';
    }

    /**
     * @param AbstractEngine $engine
     *
     * @return bool
     */
    private function needRepeat(AbstractEngine $engine) : bool
    {
        return $engine->getHttpClient()->getCode() == 0 && $this->iterationsCount < self::MAX_ITERATIONS;
    }

    /**
     * @param AbstractEngine $engine
     *
     * @throws CommonException
     */
    private function processRepeat(AbstractEngine $engine)
    {
        sleep(self::SLEEP_SECONDS);
        $this->iterationsCount++;
        $this->process($engine);
    }
}
