<?php namespace BirdPerson\Service\ContentApi;

use BirdPerson\Service\ContentApi\Engines\AbstractEngine;
use BirdPerson\Service\ContentApi\Engines\Get;
use BirdPerson\Service\ContentApi\Exceptions\CommonException;
use BirdPerson\Service\ContentApi\Requests\AbstractRequest;
use BirdPerson\Service\ContentApi\Requests\Reviews;
use BirdPerson\Service\ContentApi\Requests\Search;
use BirdPerson\Service\ContentApi\Response\Handlers\Basic as BasicResponseHandler;
use BirdPerson\Service\ContentApi\Requests\UrlTitle;
use BirdPerson\Service\ContentApi\Response\Response;

class Manager
{
    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var array
     */
    private static $methods = [
        UrlTitle::TYPE => [
            'type' => Get::TYPE,
            'method' => 'search-url-title',
        ],
        Search::TYPE => [
            'type' => Get::TYPE,
            'method' => 'search',
        ],
        Reviews::TYPE => [
            'type' => Get::TYPE,
            'method' => 'model/#/reviews',
        ],
    ];

    /**
     * @param Provider $provider
     */
    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param AbstractRequest $request
     *
     * @throws CommonException
     * @return Response
     */
    public function process(AbstractRequest $request)
    {
        $type = $request->getType();
        if (!array_key_exists($type, self::$methods)) {
            throw new CommonException('Can`t process request: undefined type ' . $type);
        }

        $response = $this->provider->process(
            $this->getEngine(self::$methods[$type])
                ->setMethod(self::$methods[$type]['method'])
                ->setData($request->getData())
        );

        if (isset($response['status']) && $response['status'] === 'error') {
            throw new CommonException('Error response with message: "' . $response['message'] . '"');
        }

        return new Response($response);
    }

    /**
     * @param array $method
     *
     * @return AbstractEngine|null
     */
    private function getEngine(array $method) : AbstractEngine
    {
        $engine = null;

        switch ($method['type']) {
            case Get::TYPE:
                $engine = new Get(new BasicResponseHandler());
                break;
        }

        return $engine;
    }
}

