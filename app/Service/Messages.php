<?php namespace BirdPerson\Service;

use BirdPerson\Entity\Message;
use BirdPerson\Exceptions\CommonException;
use BirdPerson\Job\AbstractJob;
use BirdPerson\Job\GetBash;
use BirdPerson\Job\GetReviews;
use BirdPerson\Job\GetUsage;

class Messages
{
    const TYPE_GET_REVIEWS = 1;
    const TYPE_GET_USAGE = 2;
    const TYPE_GET_BASH = 3;

    /**
     * @param Message $message
     *
     * @return AbstractJob|null
     * @throws CommonException
     */
    static public function getJobForMessage(Message $message)
    {
        switch ($message->getMessageTypeId()) {
            case (static::TYPE_GET_REVIEWS):
                return new GetReviews($message);
                break;
            case (static::TYPE_GET_USAGE):
                return new GetUsage($message);
                break;
            case (static::TYPE_GET_BASH):
                return new GetBash($message);
                break;
            default:
                return new getUsage($message);
                break;
        }
    }
}
