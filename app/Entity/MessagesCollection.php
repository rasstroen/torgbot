<?php namespace BirdPerson\Entity;

class MessagesCollection extends AbstractCollection
{
    /**
     * @param array $messagesData
     */
    public function setData(array $messagesData)
    {
        foreach ($messagesData as $data) {
            $message = new Message();
            $message->setData($data);
            $this->add($message);
        }
    }
}
