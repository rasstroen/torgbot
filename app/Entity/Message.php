<?php namespace BirdPerson\Entity;

class Message extends AbstractEntity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $messageTypeId;

    /**
     * @var int
     */
    protected $networkId;

    /**
     * @var mixed
     */
    protected $networkMessageId;

    /**
     * @var mixed
     */
    protected $chatId;

    /**
     * @var mixed
     */
    protected $authorId;

    /**
     * @var int
     */
    protected $timestamp;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $body;

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        if (!empty($data['id'])) {
            $this->setId((int)$data['id']);
        }
        if (!empty($data['timestamp'])) {
            $this->setTimestamp((int)$data['timestamp']);
        }
        if (!empty($data['message_type_id'])) {
            $this->setMessageTypeId((int)$data['message_type_id']);
        }
        if (!empty($data['network_id'])) {
            $this->setNetworkId((int)$data['network_id']);
        }
        if (!empty($data['network_message_id'])) {
            $this->setNetworkMessageId($data['network_message_id']);
        }
        if (!empty($data['chat_id'])) {
            $this->setChatId($data['chat_id']);
        }
        if (!empty($data['author_id'])) {
            $this->setAuthorId($data['author_id']);
        }
        if (null !== $data['text']) {
            $this->setText($data['text']);
        }
        if (!empty($data['body'])) {
            $this->setBody($data['body']);
        }
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getNetworkId() : int
    {
        return $this->networkId;
    }

    /**
     * @param int $networkId
     */
    public function setNetworkId(int $networkId)
    {
        $this->networkId = $networkId;
    }

    /**
     * @return mixed
     */
    public function getChatId() : string
    {
        return $this->chatId;
    }

    /**
     * @param mixed $chatId
     */
    public function setChatId(string $chatId)
    {
        $this->chatId = $chatId;
    }

    /**
     * @return mixed
     */
    public function getAuthorId() : string
    {
        return $this->authorId;
    }

    /**
     * @param mixed $authorId
     */
    public function setAuthorId(string $authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * @return int
     */
    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    /**
     * @param int $timestamp
     */
    public function setTimestamp(int $timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getBody() : string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body)
    {
        $this->body = $body;
    }

    /**
     * @param int $messageTypeId
     */
    public function setMessageTypeId(int $messageTypeId)
    {
        $this->messageTypeId = $messageTypeId;
    }

    /**
     * @return int
     */
    public function getMessageTypeId() : int
    {
        return $this->messageTypeId;
    }

    /**
     * @return mixed
     */
    public function getNetworkMessageId() : string
    {
        return $this->networkMessageId;
    }

    /**
     * @param mixed $networkMessageId
     */
    public function setNetworkMessageId(string $networkMessageId)
    {
        $this->networkMessageId = $networkMessageId;
    }
}