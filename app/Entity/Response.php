<?php namespace BirdPerson\Entity;

class Response extends AbstractEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $messageId;

    /**
     * @var int
     */
    private $networkId;

    /**
     * @var int
     */
    private $timestamp = 0;

    /**
     * @var string
     */
    private $text;

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        if (!empty($data['id'])) {
            $this->id = (int)$data['id'];
        }
        if (!empty($data['message_id'])) {
            $this->messageId = (int)$data['message_id'];
        }
        if (!empty($data['network_id'])) {
            $this->networkId = (int)$data['network_id'];
        }
        if (!empty($data['timestamp'])) {
            $this->timestamp = (int)$data['timestamp'];
        }
        if (!empty($data['text'])) {
            $this->text = $data['text'];
        }
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMessageId() : int
    {
        return $this->messageId;
    }

    /**
     * @return int
     */
    public function getNetworkId() : int
    {
        return $this->networkId;
    }

    /**
     * @return int
     */
    public function getTimestamp() : int
    {
        return $this->timestamp;
    }

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }
}