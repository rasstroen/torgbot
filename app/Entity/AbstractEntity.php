<?php namespace BirdPerson\Entity;

abstract class AbstractEntity
{
    protected $data = [];

    /**
     * @param array $data
     */
    abstract public function setData(array $data);
}
