<?php namespace BirdPerson\Entity;

use Countable;
use Iterator;

abstract class AbstractCollection extends AbstractEntity implements Iterator, Countable
{
    /**
     * @var array
     */
    protected $objects = [];

    /**
     * @param AbstractEntity $object
     */
    protected function add(AbstractEntity $object)
    {
        $this->objects[] = $object;
    }

    /**
     * @return AbstractEntity
     */
    public function current()
    {
        return current($this->objects);
    }

    /**
     * @return AbstractEntity|bool
     */
    public function next()
    {
        return next($this->objects);
    }

    /**
     * @return mixed
     */
    public function key()
    {
        return key($this->objects);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        $key = key($this->objects);

        return ($key !== null && $key !== false);
    }

    public function rewind()
    {
        reset($this->objects);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->objects);
    }
}
